# frozen_string_literal: true

# colorize string
class String
  # colorization
  define_method(:colorize) { |color_code| "\e[#{color_code}m#{self}\e[0m" }
  # ...
  define_method(:black) { "\e[30m#{self}\e[0m" }
  define_method(:red) { "\e[31m#{self}\e[0m" }
  define_method(:green) { "\e[32m#{self}\e[0m" }
  define_method(:yellow) { "\e[33m#{self}\e[0m" }
  define_method(:blue) { "\e[34m#{self}\e[0m" }
  define_method(:magenta) { "\e[35m#{self}\e[0m" }
  define_method(:cyan) { "\e[36m#{self}\e[0m" }
  define_method(:gray) { "\e[37m#{self}\e[0m" }
  # ...
  define_method(:bg_black) { "\e[40m#{self}\e[0m" }
  define_method(:bg_red) { "\e[41m#{self}\e[0m" }
  define_method(:bg_green) { "\e[42m#{self}\e[0m" }
  define_method(:bg_yellow) { "\e[43m#{self}\e[0m" }
  define_method(:bg_blue) { "\e[44m#{self}\e[0m" }
  define_method(:bg_magenta) { "\e[45m#{self}\e[0m" }
  define_method(:bg_cyan) { "\e[46m#{self}\e[0m" }
  define_method(:bg_gray) { "\e[47m#{self}\e[0m" }
  # ...
  define_method(:bold) { "\e[1m#{self}\e[22m" }
  define_method(:italic) { "\e[3m#{self}\e[23m" }
  define_method(:underline) { "\e[4m#{self}\e[24m" }
  define_method(:blink) { "\e[5m#{self}\e[25m" }
  define_method(:reverse_color) { "\e[7m#{self}\e[27m" }
  define_method(:cell) { |size = 2| self&.to_s&.rjust(size) }
end
