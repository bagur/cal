#!/usr/bin/env ruby
# frozen_string_literal: false

# loop do
#   system("stty raw -echo") #=> Raw mode, no echo
#   @char = STDIN.getc
#   system("stty -raw echo") #=> Reset terminal mode
#   print @char
#   break if @char == 'q'
# end
# puts

# require 'io/console'
 
# def yesno
#   case $stdin.getch
#     when "Y" then true
#     when "N" then false
#     else raise "Invalid character."
#   end
# end

# loop do
#   c = $stdin.getc
#   print c
#   break if c == 'q'
# end
# puts

module GetKey

  # Check if Win32API is accessible or not
  @use_stty = begin
    require 'Win32API'
    false
  rescue LoadError
    # Use Unix way
    true
  end

  # Return the ASCII code last key pressed, or nil if none
  #
  # Return::
  # * _Integer_: ASCII code of the last key pressed, or nil if none
  def self.getkey
    if @use_stty
      system('stty raw -echo') # => Raw mode, no echo
      char = (STDIN.read_nonblock(1).ord rescue nil)
      system('stty -raw echo') # => Reset terminal mode
      return char
    else
      return Win32API.new('crtdll', '_kbhit', [ ], 'I').Call.zero? ? nil : Win32API.new('crtdll', '_getch', [ ], 'L').Call
    end
  end

end
# loop do
#   k = GetKey.getkey
#   puts "Key pressed: #{k.inspect}"
#   sleep 1
# end

module GetButton
  def self.getkey
    require 'io/console'

    system('stty raw -echo')
    k = $stdin.getch
    system('stty -raw echo')
    k
  end
end
