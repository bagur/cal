#!/usr/bin/env ruby
# frozen_string_literal: false

require 'date'
require './color_string'
require './getkey'

# class
class Cal
  def initialize
    # days of week hash
    @dow = %w[Su Mo Tu We Th Fr Sa Su]
    @today = Date.today
    @current = @today.dup
    @weekline = make_weekline # :sm, :md
    @ds = Date.new(@current.year, @current.month, 1)
    @de = @ds.next_month.prev_day
    @type = 1
  end

  def cell_date(dat)
    c = dat.day.to_s.cell
    c = c.red if dat.sunday? || dat.saturday?
    c = if dat == @current
          if dat == @today
            "[#{c}]".bg_red
          else
            '['.yellow + c + ']'.yellow
          end
        else
          if dat == @today
            " #{c} ".bg_red
          else
            " #{c} "
          end
        end
    c
  end

  def make_month
    m = []
    ds = @ds - @ds.cwday
    (1..48).each do |i|
      m.append(cell_date(ds))
      ds += 1 unless (i % 8).zero?
    end
    m
  end

  def show_month
    m = make_month
    puts m[@type, 7].join if m[@type, 7].include?(cell_date(@ds))
    puts m[@type + 8, 7].join, m[@type + 16, 7].join
    puts m[@type + 24, 7].join, m[@type + 32, 7].join
    puts m[@type + 40, 7].join if m[@type + 40, 7].include?(cell_date(@de))
    print " #{@current} ".center(28, '-')
  end

  def show_weekline
    line = make_weekline
    print line[@type, 7].join
    puts
  end

  def show
    system 'clear'
    print @current.strftime(' %B').ljust(23), @current.strftime('%Y ')
    puts
    show_weekline
    hr = '---------------------------'
    puts @type == 1 ? "+#{hr}" : "#{hr}+"
    show_month
  end

  def first_day
    Date.new(@current.year, @current.month, 1)
  end

  def last_day
    first_day.next_month.prev_day
  end

  def type
    @type = @type == 1 ? 0 : 1
  end

  def move(move)
    @current += move
    @ds = Date.new(@current.year, @current.month, 1)
    @de = @ds.next_month.prev_day
  end

  private

  def week(str)
    str.rjust(28)
  end

  def make_weekline
    s = []
    @dow.each_with_index do |v, i|
      s << case i
           when 0, 6, 7 then " #{v.cell.red} "
           else " #{v.cell} "
           end
    end
    s
  end
end

cal = Cal.new

d = Date.today
ds = Date.new(d.year, d.month, 1)
@ds = ds - ds.cwday
@df = ds.next_month.prev_day + (7 - ds.next_month.prev_day.cwday)
# puts @ds, @df

month = []
6.times do |i|
  w = []
  8.times do |j|
    w[j] = @ds.day
    @ds += 1
  end
  # puts "#{i} #{w.inspect}"
  @ds -= 1
  month[i] = w
end

week = month[0][1..7]
if week.include?(ds.day)
  week[0..6].each do |day|
    print "#{day} ".rjust(4)
  end
  puts
end
month[1..4].each do |week|
  # print v.inspect
  week[1..7].each do |day|
    print "#{day} ".rjust(4)
  end
  puts
end
week = month[5][1..7]
if week.include?(ds.next_month.prev_day.day)
  week[0..6].each do |day|
    print "#{day} ".rjust(4)
  end
  puts
end

loop do
  cal.show
  key = GetButton.getkey
  case key.ord
  when 13, 113 then break
  when 116 then cal.type
  when 65 then cal.move(-7)
  when 66 then cal.move(7)
  when 67 then cal.move(1)
  when 68 then cal.move(-1)
  else print "#{key}[#{key.ord}]"
  end
end
puts

#  1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1   1 2 1
#  0 | 2   0  /2   0  -2   0   2   0   2   0   2   0   2   0   2   0   2   0   2   0-  2   0\  2
#  9 o 3   9 o 3   9 o 3   9 o-3   9 o 3   9 o 3   9 o 3   9 o 3   9 o 3   9-o 3   9 o 3   9 o 3
#  8   4   8   4   8   4   8   4   8  -4   8  \4   8 | 4   8/  4   8-  4   8   4   8   4   8   4
#  7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5   7 6 5
